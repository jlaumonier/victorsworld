#!/bin/bash

pip3 install -r requirements-dev.txt
pyinstaller ../main.py -n victorsworld --add-data "../data/config/:data/config/" --add-data "../data/:data/" --add-data "../data/images/:data/images/"
