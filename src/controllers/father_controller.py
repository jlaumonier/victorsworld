import random

from pymasep.common import Controller, State, Agent, Environment
from ..game.vw_game import VWGame


class FatherController(Controller):
    """
    Random action controller for mouse or cat
    :param environment: the interface environment
    """

    def __init__(self, environment: Environment):
        super().__init__(environment)
        self.environment = environment

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose a random action.
        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        result = None
        q = agent.belief._create_request(c1=('agent', 'State.player0'),
                                         r=('tell',),
                                         c2=('sentence', '*'))
        r_query_s = agent.belief.query(q)
        sentences = [d['v'] for n, d in r_query_s.nodes(data=True) if d['c'] == 'sentence']
        next_dialog_choice = 'd1'
        if len(sentences) > 0:
            dlg_choices = self.environment.game.DIALOG1[sentences[-1]]['choice']
            if len(dlg_choices) > 0:
                next_dialog_choice = dlg_choices[0]
            else:
                next_dialog_choice = None
        if next_dialog_choice is not None:
            action_type = VWGame.ACTION_TALK
            result = self.environment.create_action(action_type)
            result.add_parameter('content', next_dialog_choice)

        return result
