import random

from pymasep.common import Controller, State, Agent, Environment
from ..game.vw_game import VWGame


class RandomActionController(Controller):
    """
    Random action controller for mouse or cat
    :param environment: the interface environment
    """

    def __init__(self, environment: Environment):
        super().__init__(environment)
        self.environment = environment

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose a random action.
        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        action_type = random.choice([VWGame.ACTION_NORTH,
                                     VWGame.ACTION_SOUTH,
                                     VWGame.ACTION_EAST,
                                     VWGame.ACTION_WEST])
        result = self.environment.create_action(action_type)
        return result
