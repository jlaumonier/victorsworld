from mimetypes import inited
from typing import Dict, Tuple
import os

from omegaconf import DictConfig

import pymasep as pm
from pymasep.common.state import State
from pymasep.common.action import Action
from pymasep.common.universe import Map
from pymasep.common.initializer import InitializerFixed, InitializerCreator
from pymasep.common.data import DataTMX


class VWGame(pm.common.Game):
    ACTION_NORTH = 1
    ACTION_SOUTH = 2
    ACTION_EAST = 3
    ACTION_WEST = 4
    ACTION_TALK = 5

    DIALOG1 = {
        'd1': {'text': 'Leve toi ! N''oublies pas que tu dois aller me chercher du minerai chez Rasal, a la mine.',
               'choice': ['d2', 'd4']},
        'd2': {'text': 'Oui, je sais. C''est prévu.',
               'choice': ['d3']},
        'd3': {'text': 'Parfait. Tu prendra sur la table la nouriture que ta mère t\'a préparé',
               'choice': ['d6']},
        'd4': {'text': 'Hum..',
               'choice': ['d5']},
        'd5': {
            'text': 'Je vois, encore de mauvaise humeur. Tu prendra sur la table la nouriture que ta mère t\'a préparé',
            'choice': ['d6']},
        'd6': {'text': 'C''est bon, je vais le faire.',
               'choice': ['']}
    }

    def define_coord_methods(self):
        """
        Define the dictionary of coordination methods according to the game phase
        """
        super().define_coord_methods()
        self.coord_method['intro'] = VWGame.MULTIPLAYER_COORDINATION_FREE_FOR_ALL
        self.coord_method['play'] = VWGame.MULTIPLAYER_COORDINATION_FREE_FOR_ALL

    def load_map(self):
        """
        Load the static and initial data of the game.
        """
        self.map = Map(data_path=os.path.join(self.config['root_path'], 'data', 'assets'))
        self.map.read_tmx('victorsworld.tmx',
                          [{'layers': ['objects'], 'transform': DataTMX.layer_to_list, 'name_feature': 'objects'},
                           {'layers': ['init_positions'], 'transform': DataTMX.layer_to_list,
                            'name_feature': 'init_pos'}])

    def add_initializers(self):
        """
        This method adds initializers definitions to the game.
        """

        super().add_initializers()
        posPlayer = [p[1] for p in self.map.feature_data['init_pos'] if p[0] == 'player'][0]
        posFather = [p[1] for p in self.map.feature_data['init_pos'] if p[0] == 'father'][0]
        _ = InitializerCreator.create(self, {'name': 'InterfacePos', 'value_type': "tuple", 'value': posPlayer})
        _ = InitializerCreator.create(self, {'name': 'NpcPos', 'value_type': "tuple", 'value': posFather})
        _ = InitializerCreator.create(self,{'name': 'FatherName', 'value_type': "str", 'value': "Raglar"})
        _ = InitializerCreator.create(self, {'name': 'PlayerName', 'value_type': "str", 'value': "Amraar"})
        agent_charac_initializer = {'Pos': {'_init': "InterfacePos"}, 'Name': {'_init': 'PlayerName'}}
        npc_charac_initializer = {'Pos': {'_init': "NpcPos"}, 'Name': {'_init': 'FatherName'}}

        _ = InitializerCreator.create(self, {'name': 'InterfaceAgentObjectState',
                                             '_type': 'ObjectState',
                                             '_interface': True,
                                             "sub_init": agent_charac_initializer})
        _ = InitializerCreator.create(self, {'name': 'NpcObjectState',
                                             '_type': 'ObjectState',
                                             '_interface': True,
                                             "sub_init": npc_charac_initializer})
        _ = InitializerCreator.create(self, {'name': 'PlayerInitializer',
                                             '_type': 'Object',
                                             '_interface': True,
                                             "sub_init": {'objectstate': {'_init': "InterfaceAgentObjectState"}}})

        _ = InitializerCreator.create(self, {'name': 'NpcInitializer',
                                             '_type': 'Object',
                                             '_interface': True,
                                             "sub_init": {'objectstate': {'_init': "NpcObjectState"}}})

    def init_state(self, environment) -> State:
        """
        Create the initial state of the game. Can be reimplemented in subclasses.

        :param environment: the environment
        :return: the initial state
        """
        result = super().init_state(environment)

        # create objects
        for id_d, tmx_obj in enumerate(self.map.feature_data['objects']):
            tmplt = environment.game.templates[tmx_obj[0]]
            obj = environment.create_object(name=tmx_obj[0] + str(id_d), template=tmplt)
            obj.object_state.characteristics['Pos'].value = tmx_obj[1]
            result.add_object(obj)

        self.set_system_value(result, 'GamePhase', 'intro')
        return result

    @classmethod
    def update_belief(cls, agent_fname: str, observation: State, state: State) -> None:
        # update talk beliefs
        for ag in observation.agents:
            sentence = ag.object_state.characteristics['Talk'].value
            if sentence != '':
                obs_agent = state.objects[agent_fname]
                obs_agent.belief.add(c1=('agent', ag.get_fullname()), r=('tell',), c2=('sentence', sentence))

    def next_state(self, current_state: State, actions: Dict[str, Action]) -> Tuple[State, Dict]:
        """
        Calculates the new state according to the current state and the action of the agent.

        :param current_state: the current state
        :param actions: action of the agents
        :return: the new state and the additional information about the state (may be None if no additional information)
        """

        def _is_pos_contain_object(pos: tuple) -> bool:
            contain_result = False
            for obj in current_state.objects.values():
                if 'Pos' in obj.object_state.characteristics:
                    if obj.object_state.characteristics['Pos'].value == pos:
                        contain_result = True
            return contain_result

        result = current_state

        result_additional_info = {'state_changed': False}

        for ag in result.agents:
            if ag.get_fullname() in actions.keys():
                action = actions[ag.get_fullname()]
                if VWGame.get_system_value(result, 'GamePhase') == 'intro':
                    if action.type == VWGame.ACTION_VIDEO_ENDED:
                        VWGame.set_system_value(result, 'GamePhase', 'play')
                        result_additional_info['state_changed'] = True
                else:
                    # Remove talk for all actions by default (Waiting for Action Duration)
                    ag.object_state.characteristics['Talk'].value = ''

                    if action.type == VWGame.ACTION_INTENTION:
                        action_intention = action.params['action']
                        if action_intention.type == VWGame.ACTION_TALK:
                            result_additional_info['state_changed'] = True
                            if ag.intention is None:
                                ag.intention = current_state.environment.create_object(name="intention",
                                                                                       template=self.templates[
                                                                                           'EmptyIntentionTemplate'])
                                ag.intention.add_intention(action_intention)
                                # query agent belief to see what father said
                                q = ag.belief._create_request(c1=('agent', 'State.father'),
                                                              r=('tell',),
                                                              c2=('sentence', '*'))
                                r_query_s = ag.belief.query(q)
                                sentences = [d['v'] for n, d in r_query_s.nodes(data=True) if d['c'] == 'sentence']
                                # TODO Should have a better way to select the last told sentences than -1 which requires the query to return ordered sentences.
                                next_dialog_choice = self.DIALOG1[sentences[-1]]['choice']
                                ag.intention.intentions[0].params['talk_choice'] = next_dialog_choice

                    if action.type in [VWGame.ACTION_NORTH,
                                       VWGame.ACTION_SOUTH,
                                       VWGame.ACTION_EAST,
                                       VWGame.ACTION_WEST]:
                        pos_player = ag.object_state.characteristics['Pos'].value
                        res_pos_charac = ag.object_state.characteristics['Pos']
                        new_pos = pos_player
                        if action.type == VWGame.ACTION_EAST:
                            new_pos = (pos_player[0] + 1, pos_player[1])
                        if action.type == VWGame.ACTION_WEST:
                            new_pos = (pos_player[0] - 1, pos_player[1])
                        if action.type == VWGame.ACTION_SOUTH:
                            new_pos = (pos_player[0], pos_player[1] + 1)
                        if action.type == VWGame.ACTION_NORTH:
                            new_pos = (pos_player[0], pos_player[1] - 1)
                        if not _is_pos_contain_object(new_pos):
                            res_pos_charac.value = new_pos
                            result_additional_info['state_changed'] = True
                    if action.type == VWGame.ACTION_TALK:
                        ag.object_state.characteristics['Talk'].value = action.params['content']
                        # remove intention if exists
                        if ag.intention is not None:
                            ag.intention = None
                        result_additional_info['state_changed'] = True

        return result, result_additional_info
