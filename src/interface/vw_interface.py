import os
from typing import Optional

from omegaconf import DictConfig
import pygame

import pymasep as pm
from pymasep.interface import CutSceneInterfaceState
from pymasep.application import ConnectedSubAppInfo
from pymasep.interface import GraphicalResources
from pymasep.common.data import DataTMX
from pymasep.common import Object, Agent
from src.game import VWGame
from .vw_interface_state_run import VWInterfaceStateRun
from .vw_player_controller import VWPlayerController


class VWInterface(pm.interface.Interface):

    def __init__(self, app, received_q_id: int, cfg: DictConfig = None,
                 remote_engine_host: Optional[str] = None,
                 remote_engine_port: Optional[int] = None,
                 search_remote: bool = False):
        super().__init__(app, received_q_id, cfg, VWGame, ConnectedSubAppInfo.ROLE_ACTOR,
                         remote_engine_host, remote_engine_port, search_remote)
        self.data_dir = os.path.join(self.root_path, 'data')

        self.gr = None
        """ Graphical ressources """

        self.controller_interface_class_ = VWPlayerController
        self.backgroundColor = (125, 125, 125)
        self.virtual_screen_size = (1368, 768)

        self.ui_theme_file = 'data/pygame_ui_theme.json'

        self.square_size = (50, 50)

        self.app_states[VWInterfaceStateRun.__name__] = VWInterfaceStateRun(self)
        self.app_states['Introduction'] = CutSceneInterfaceState(self,
                                                                 os.path.join(self.data_dir,'intro/intro.mpg'),
                                                                 os.path.join(self.data_dir,'music/intro.mp3'),
                                                                 'intro')

    def load_resources(self):
        """ load the graphical resources """

        def _load_sub_image(image, rect):
            sub_img = image.subsurface(rect)
            return sub_img

        super().load_resources()
        self.resources['background'] = pygame.Surface(self.virtual_screen_size).convert_alpha()
        self.resources['background'].fill(self.backgroundColor)

        self.gr = GraphicalResources(data_path=os.path.join(self.config['root_path'], 'data', 'assets'))
        self.gr.read_tmx(filename='victorsworld.tmx',
                         transforms=[{'layers': ['background'],
                                      'transform': DataTMX.layer_to_surface,
                                      'store_key': 'background'}])

        self.resources['wall'] = self.gr.tilesets[0]['tile_surface'][29]
        self.resources['door'] = self.gr.tilesets[0]['tile_surface'][36]
        self.resources['bedup'] = self.gr.tilesets[0]['tile_surface'][32]
        self.resources['beddown'] = self.gr.tilesets[0]['tile_surface'][39]
        self.resources['cabinetul'] = self.gr.tilesets[0]['tile_surface'][45]
        self.resources['cabinetur'] = self.gr.tilesets[0]['tile_surface'][46]
        self.resources['cabinetdl'] = self.gr.tilesets[0]['tile_surface'][52]
        self.resources['cabinetdr'] = self.gr.tilesets[0]['tile_surface'][53]

        self.resources['player0'] = pygame.image.load(os.path.join(self.data_dir,
                                                                   'images/character.png'))
        self.resources['father'] = pygame.image.load(os.path.join(self.data_dir,
                                                                  'images/npc.png'))

    def get_resource_from_object(self, obj: Object) -> Optional[pygame.Surface]:
        """
        Get a surface to display an object

        :param obj: The object to display

        :return: The surface to display
        """
        if type(obj) == Agent:
            token_name = obj.name
        else:
            token_name = obj.nature
        if token_name in self.resources:
            return self.resources[token_name]
        else:
            return None

    def init_game(self):
        """
        Initialize game : Connect to engine, define the game and register self as a new interface. Start the Run state
        """
        super().init_game()
        # first app state is init
        self.push_state(self.app_states[VWInterfaceStateRun.__name__])
