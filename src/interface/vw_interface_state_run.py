import pygame
from pygame.locals import *

from pymasep.interface import InterfaceState, UIDialogBox, user_event
from src.game import VWGame
from src.interface.vw_iu_board import VWUIBoard


class VWInterfaceStateRun(InterfaceState):

    def __init__(self, sub_application):
        super().__init__(sub_application=sub_application,
                         container_dim=sub_application.virtual_screen_size)
        self.ui_map = None

    def init(self):
        """
        Init the run state : init Player interface elements
        """
        super().init()
        self.ui_map = VWUIBoard(relative_rect=pygame.Rect((0, 0),
                                                          self.sub_application.resources['background'].get_size()),
                                manager=self.sub_application.ui_manager,
                                container=self.ui_container,
                                sub_application=self.sub_application,
                                image_surface=self.sub_application.resources['background'],
                                game_data=None,
                                game_class_=VWGame,
                                origin_coord_map=(0, 0),
                                square_size=self.sub_application.square_size,
                                map_size=(10,10),
                                player_name='player0'
                                )
        self.ui_map.set_mode(VWUIBoard.RUN_MODE)

    def handle_event(self, event) -> None:
        """
        Handle events in the state

        :param event: the event to handle
        """
        if event.type == KEYDOWN:
            if event.key == K_t:
                evt_intention_talk = pygame.event.Event(user_event.INTENTION_TALK_EVENT)
                pygame.event.post(evt_intention_talk)

    def update(self):
        """ Update elements to render them. Go to 'intro' state if PhaseGame is 'intro' """
        super().update()
        current_state = self.sub_application.environment.current_state
        game = self.sub_application.environment.game
        if current_state is not None and game.get_system_value(current_state, 'GamePhase') == 'intro':
            self.sub_application.push_state(self.sub_application.app_states['Introduction'])

    def on_receive_observation(self):
        """ Executed on receive observation. Update game logs"""
        self.ui_map.player_name = self.sub_application.environment.controlled_agent.get_fullname()



