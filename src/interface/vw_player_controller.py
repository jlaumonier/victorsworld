from pygame.locals import *
from pymasep.common import State, Agent, Environment
from pymasep.interface import InterfaceController
import pymasep.interface.user_event as user_event
from src.game import VWGame


class VWPlayerController(InterfaceController):
    """
    Interface controller that transform interface event to player action/intentions

    :param environment: the interface environment
    """

    def __init__(self, environment: Environment):
        super().__init__(environment)
        self.environment = environment
        # action chosen by event
        self.action = None

    def action_choice(self, observation: State, agent: Agent):
        """
        Choose the action. (Default, at the moment : random)
        :param observation the observation used to choose the action
        :param agent the agent who choose the action
        :return the action chosen for the agent.
        """
        result = self.action
        self.action = None
        return result

    def select_action(self, event):
        """
        Selection action according to the interface event
        :param event: the event.
        :return: true if the event has been processed
        """
        self.action = None
        event_processed = False
        if event.type == KEYDOWN:
            if event.key == K_DOWN:
                self.action = self.environment.create_action(VWGame.ACTION_SOUTH)
                event_processed = True
            if event.key == K_UP:
                self.action = self.environment.create_action(VWGame.ACTION_NORTH)
                event_processed = True
            if event.key == K_LEFT:
                self.action = self.environment.create_action(VWGame.ACTION_WEST)
                event_processed = True
            if event.key == K_RIGHT:
                self.action = self.environment.create_action(VWGame.ACTION_EAST)
                event_processed = True

        if event.type == user_event.INTENTION_TALK_EVENT:
            self.action = self.environment.create_action(VWGame.ACTION_INTENTION)
            action_intention = self.environment.create_action(VWGame.ACTION_TALK)
            self.action.params['action'] = action_intention
            event_processed = True

        if event.type == user_event.TALK_EVENT:
            self.action = self.environment.create_action(VWGame.ACTION_TALK)
            self.action.add_parameter('content', event.dlg_id)
            # TODO Should remove the intention if exists ? The Game only remove the intention
            event_processed = True

        if event.type == user_event.VIDEO_ENDED_EVENT:
            self.action = self.environment.create_action(VWGame.ACTION_VIDEO_ENDED)
            event_processed = True

        return event_processed
