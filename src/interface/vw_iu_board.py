from pygame import Rect, Surface
from pygame_gui import TEXT_EFFECT_TYPING_APPEAR
from pygame_gui.elements import UITextBox, UITooltip
from pygame.event import Event
from pygame_gui.core.interfaces import IUIManagerInterface, IContainerLikeInterface

from pymasep.application import SubApp
from pymasep.interface.ui_board import UIBoard
from pymasep.interface.ui_dialog_box import UIDialogBox
from pymasep.interface.ui_talk import UITalk
from pymasep.interface import user_event


class VWUIBoard(UIBoard):

    def __init__(self, relative_rect: Rect,
                 manager: IUIManagerInterface,
                 container: IContainerLikeInterface,
                 sub_application: SubApp,
                 image_surface: Surface,
                 game_data,
                 game_class_,
                 origin_coord_map,
                 square_size,
                 map_size,
                 player_name
                 ):
        super().__init__(relative_rect, manager, container, sub_application, image_surface, game_data,
                         game_class_, origin_coord_map, square_size, map_size, player_name)
        self.dialog_box = None
        self.talk_ui = {}

    def process_event(self, event: Event) -> bool:
        """
        Process an event on the board.

        :param event: the event to process
        :return: True if it has been processed
        """
        event_processed = super().process_event(event)
        if event.type in [user_event.INTENTION_TALK_EVENT, user_event.TALK_EVENT]:
            event_processed = self.sub_application.environment.controlled_agent.controller.select_action(
                event) or event_processed
        return event_processed

    def update_with_observation(self, time_delta: float):
        super().update_with_observation(time_delta)

        state = self.sub_application.environment.current_state

        player_intention = state.objects[self.player_name].intention
        if player_intention is not None:
            validated_action = player_intention.intentions[0]
            list_id_choice = validated_action.params['talk_choice']
            list_choice = [(dlg_id, self.sub_application.environment.game.DIALOG1[dlg_id]['text']) for dlg_id in
                           list_id_choice]
            if self.dialog_box is None:
                max_len = max([len(x[1]) for x in list_choice])
                size_font = 16
                self.dialog_box = UIDialogBox(rect=Rect(100, 400, max_len * size_font, 200),
                                              ui_manager=self.ui_manager,
                                              choice=list_choice)
        else:
            if self.dialog_box is not None:
                self.dialog_box.kill()
                self.dialog_box = None

        for obj in state.objects.values():
            characs = obj.object_state.characteristics
            if 'Talk' in characs:
                content_id = characs['Talk'].value
                content = ''
                if content_id == '':
                    if obj.get_fullname() in self.talk_ui.keys():
                        self.talk_ui[obj.get_fullname()].kill()
                        del self.talk_ui[obj.get_fullname()]
                else:
                    content = self.sub_application.environment.game.DIALOG1[content_id]['text']
                    if obj.get_fullname() in self.talk_ui.keys():
                        if self.talk_ui[obj.get_fullname()].html_text != content:
                            self.talk_ui[obj.get_fullname()].kill()
                            del self.talk_ui[obj.get_fullname()]

                if content != '':
                    max_width = 200
                    pos = list(self.objects_tokens[obj.get_fullname()].board_pos)
                    pos[0] += 1
                    if obj.get_fullname() not in self.talk_ui.keys():
                        talk_label = UITalk(manager=self.ui_manager,
                                            container=self.ui_container,
                                            relative_rect=Rect(self.map_pos_to_surface_pos(tuple(pos)),
                                                               (max_width + 10, -1)),
                                            html_text=content,
                                            speaker_name=characs['Name'].value)
                        self.talk_ui[obj.get_fullname()] = talk_label
