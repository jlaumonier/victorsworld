import os
import sys

from numpy import ndarray  # not usefull but needed by pyinstaller (windows) to be imported

from pymasep.app import App
from src.interface import VWInterface
from src.controllers import RandomActionController  # not usefull but needed by pyinstaller to be imported


def main():
    """
    Main function. Launch Engine and Interface on the same process
    """

    root_path = getattr(sys, '_MEIPASS', os.path.abspath(os.path.join(os.path.dirname(__file__))))
    application = App(VWInterface, root_path=root_path, config_path='data/config')
    application.run()


if __name__ == "__main__":
    main()
