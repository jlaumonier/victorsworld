import sys
import time
import numpy as np
from codecarbon import EmissionsTracker

import pygame
from pygame.locals import *


def fps_counter(fps, avg_fps, surface, font_small):
    fps_t = font_small.render(f'{fps:>10.0f}' , 0, pygame.Color("RED"))
    avg_fps_t = font_small.render(f'{avg_fps:>10.0f}', 0, pygame.Color("RED"))
    surface.blit(fps_t,(0,0))
    surface.blit(avg_fps_t, (40, 0))


def consumption(emmission, surface, font_small):
    cpu_energy = emmission['cpu_energy_consumed']
    kw = font_small.render(f'{cpu_energy} Kwh' , 0, pygame.Color("BLUE"))
    surface.blit(kw, (200, 0))


def load_sub_image(image, rect):
    result = pygame.Surface(rect.size).convert(image)
    result.blit(image, (0,0), rect)
    colorkey = result.get_at((1, 1))
    result.set_colorkey(colorkey, pygame.RLEACCEL)
    return result


def load_ressources():
    # thanks to Master484, http://m484games.ucoz.com/, https://opengameart.org/content/space-soldier-m484-games
    sprites_src = pygame.image.load("M484SpaceSoldier.png")
    sprites_anim = []
    sprite_size = (50, 50)
    sprite_coord = (8, 67)
    for idx_sprite in range(8):
        one_sprite = load_sub_image(sprites_src,
                                    pygame.Rect((sprite_coord[0]+(idx_sprite*(sprite_size[0]+1)), sprite_coord[1]),
                                                 sprite_size)
                                    )
        sprites_anim.append(one_sprite)
    return sprites_anim


# see https://github.com/mlco2/codecarbon/issues/438#issuecomment-1665573581
# warning. hight  ressource comsumption ! 200 fps => 20 fps
# Need to be colled only every minute or so.
def get_emissions_data(tracker):
    _ = tracker.flush()
    emissions_data = tracker._prepare_emissions_data()

    if not emissions_data.emissions:
        raise Exception("No emissions data found.")

    return {
        "cpu_power": emissions_data.cpu_power / 1000,
        "gpu_power": emissions_data.gpu_power / 1000,
        "ram_power": emissions_data.ram_power / 1000,
        "cpu_energy_consumed": tracker._total_cpu_energy.kWh,
        "gpu_energy_consumed": tracker._total_gpu_energy.kWh,
        "ram_energy_consumed": tracker._total_ram_energy.kWh,
        "total_energy_consumed": tracker._total_energy.kWh,
        "co2_emissions": emissions_data.emissions,
    }

class Player(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.sprites = load_ressources()
        self.cur_sprite_idx = 0
        self.pos = [0, 50]
        self.fpos = [0.0, 50.0]
        self.max_time_animation = 1000  # ms
        self.sprite_duration = self.max_time_animation  / len(self.sprites)
        self.max_distance_animation = 150 # pixel
        self.velocity = self.max_distance_animation / self.max_time_animation
        self.total_time_animation = 0
        self.start = time.time()
        self.start_animation_px = self.pos[0]


    def render(self, display, time_delta):
        if (self.total_time_animation + time_delta) >= self.max_time_animation:
            self.cur_sprite_idx = 0
            self.total_time_animation = 0
        self.cur_sprite_idx = ((self.total_time_animation + time_delta) * len(self.sprites)) / self.max_time_animation
        # print(self.total_time_animation + time_delta, len(self.sprites), self.cur_sprite_idx, int(self.cur_sprite_idx))
        self.cur_sprite_idx = int(self.cur_sprite_idx)
        self.total_time_animation += time_delta
        display.blit(self.sprites[self.cur_sprite_idx], self.pos)

    def update(self, time_delta):
        self.fpos[0] += self.velocity * time_delta
        print(time.time() - self.start, self.fpos[0], self.velocity, time_delta)
        if self.pos[0] >= self.max_distance_animation + self.start_animation_px:
            self.fpos[0] = 0.0
            self.start_animation_px = int(self.fpos[0])
            print(time.time() - self.start, self.pos[0], "fin")
            self.start = time.time()
        self.pos = (int(self.fpos[0]), int(self.fpos[1]))


def main(tracker):

    # Predefined some colors
    BLUE  = (0, 0, 255)
    RED   = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)

    pygame.init()
    SCREEN_SIZE = (640, 480)
    display = pygame.display.set_mode(SCREEN_SIZE, flags=0)
    end = False

    MAX_FPS = 30

    font_small = pygame.font.SysFont("Verdana", 20)

    # Thanks https://stackoverflow.com/questions/71747585/different-frame-rate-calculation-methods-produce-very-different-results
    window_fps = []
    clock = pygame.time.Clock()

    player = Player()

    ENERGY_EVENT = pygame.USEREVENT + 1
    pygame.time.set_timer(ENERGY_EVENT, 10000)
    emissions = get_emissions_data(tracker)

    while not end:
        time_delta = clock.tick(MAX_FPS)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                end = True
            if event.type == ENERGY_EVENT:
                emissions = get_emissions_data(tracker)

        if not end:

            player.update(time_delta)

            # FPS
            current_fps = 1000 / time_delta
            window_fps.append(current_fps)
            if len(window_fps) == 1000:
                del window_fps[0]
            avg_fps = np.around(sum(window_fps) / len(window_fps))

            display.fill(WHITE)
            player.render(display, time_delta)
            fps_counter(current_fps, avg_fps, display, font_small)
            consumption(emissions, display, font_small)
            pygame.display.update()


if __name__ == "__main__":
    with EmissionsTracker(output_dir='.', log_level='error') as tracker:
        main(tracker)