# Tutoriel https://coderslegacy.com/python/python-pygame-tutorial/
import pygame
from pygame.locals import *
import random
import time

# Predefined some colors
BLUE  = (0, 0, 255)
RED   = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Screen information
#SCREEN_SIZE = (3840, 2160)  # 4K
#SCREEN_SIZE = (2560, 1440)  # QHD
#SCREEN_SIZE = (1920, 1080)  # Full HD
#SCREEN_SIZE = (1024, 768)  #
#SCREEN_SIZE = (800, 600)  #
#SCREEN_SIZE = (640, 480)  #
#SCREEN_SIZE = (320, 200)  #
SCREEN_SIZE = (400, 600) # Tutorial

SCREEN_WIDTH = SCREEN_SIZE[0]
SCREEN_HEIGHT = SCREEN_SIZE[1]

SPEED = 5
SCORE = 0

FPS = 60

pygame.init()
FramePerSec = pygame.time.Clock()
display = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
display.fill(WHITE)
pygame.display.set_caption("Game")
end = False

font = pygame.font.SysFont("Verdana", 60)
font_small = pygame.font.SysFont("Verdana", 20)
game_over = font.render("Game Over", True, BLACK)

background = pygame.image.load("AnimatedStreet.png")

def fps_counter(surface):
    fps = str(int(FramePerSec.get_fps()))
    fps_t = font_small.render(fps , 0, pygame.Color("RED"))
    surface.blit(fps_t,(SCREEN_WIDTH-50,0))


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Enemy.png")
        self.rect = self.image.get_rect()
        self.rect.center = (random.randint(40, SCREEN_WIDTH - 40), 0)

    def move(self):
        global SCORE
        self.rect.move_ip(0, SPEED)
        if (self.rect.bottom > SCREEN_HEIGHT):
            SCORE += 1
            self.rect.top = 0
            self.rect.center = (random.randint(30, SCREEN_WIDTH - 40), 0)

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Player.png")
        self.rect = self.image.get_rect()
        self.rect.center = (160, 520)

    def move(self):
        pressed_keys = pygame.key.get_pressed()
        # if pressed_keys[K_UP]:
        #     self.rect.move_ip(0, -5)
        # if pressed_keys[K_DOWN]:
        #     self.rect.move_ip(0,5)
        if self.rect.left > 0:
            if pressed_keys[K_LEFT]:
                self.rect.move_ip(-5, 0)
        if self.rect.right < SCREEN_WIDTH:
            if pressed_keys[K_RIGHT]:
                self.rect.move_ip(5, 0)


Player1 = Player()
Enemy1 = Enemy()

#Creating Sprites Groups
enemies = pygame.sprite.Group()
enemies.add(Enemy1)
all_sprites = pygame.sprite.Group()
all_sprites.add(Player1)
all_sprites.add(Enemy1)

#Adding a new User event
INC_SPEED = pygame.USEREVENT + 1
pygame.time.set_timer(INC_SPEED, 1000)

while not end:
    for event in pygame.event.get():
        if event.type == INC_SPEED:
            SPEED += 2
        if event.type == QUIT:
            pygame.quit()
            end = True
    if not end:
        display.blit(background, (0,0))
        scores = font_small.render(str(SCORE), True, BLACK)
        display.blit(scores, (10, 10))
        # Moves and Re-draws all Sprites
        for entity in all_sprites:
            entity.move()
            display.blit(entity.image, entity.rect)

        # To be run if collision occurs between Player and Enemy
        if pygame.sprite.spritecollideany(Player1, enemies):
            pygame.mixer.Sound('crash.wav').play()
            time.sleep(0.5)
            display.fill(RED)
            display.blit(game_over, (30, 250))
            pygame.display.update()
            for entity in all_sprites:
                entity.kill()
            time.sleep(2)
            pygame.quit()
            end = True

        fps_counter(display)
        pygame.display.update()
        FramePerSec.tick(FPS)



