import pygame
from codecarbon import EmissionsTracker
from pygame.locals import *
import os
import pygame_gui

def display_textbitmap(text: str, font) -> pygame.Surface:
    s = font.render(text, 0, (255, 240, 230))
    return s

def main():

    # Predefined some colors
    BLUE  = (0, 0, 255)
    RED   = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)

    pygame.init()
    SCREEN_SIZE = (640, 480)
    display = pygame.display.set_mode(SCREEN_SIZE, flags=0)
    end = False

    manager = pygame_gui.UIManager(SCREEN_SIZE,'mdv-61-theme.json')

    MAX_FPS = 30

    text = 'Toto est gentil'

    clock = pygame.time.Clock()

    text_label = pygame_gui.elements.UILabel(relative_rect=Rect(20, 20, 600, 200),
                                             text=text,
                                             manager=manager,
                                             object_id='#big_font')
    text_label.set_active_effect(pygame_gui.TEXT_EFFECT_TYPING_APPEAR, params={'time_per_letter':0.5})

    while not end:
        time_delta = clock.tick(MAX_FPS)/1000.0
        for event in pygame.event.get():
            if event.type == QUIT:
                end = True
            manager.process_events(event)

        manager.update(time_delta)
        manager.draw_ui(display)
        pygame.display.update()

    pygame.quit()

if __name__ == "__main__":
    main()