import os
import pytest

from omegaconf import OmegaConf
from pymasep.common import Environment, Action, State

from src.game import VWGame

@pytest.fixture(scope="function")
def relative_path():
    yield '../'


@pytest.fixture(scope="function")
def configuration(base_directory):
    cfg = OmegaConf.load(os.path.join(base_directory, 'config', 'game_one_agent', 'app.yaml'))
    cfg['root_path'] = base_directory
    yield cfg


def test_next_state(mock_environment_conf):
    env = mock_environment_conf
    current_state = env.game.initialize_state(env)
    action = {'State.player0': Action(env, VWGame.ACTION_SOUTH)}

    new_state, additional_info = env.game.next_state(current_state, action)

    assert isinstance(new_state, State)
    assert new_state.objects['State.player0'].object_state.characteristics['Pos'].value == (0, 1)
