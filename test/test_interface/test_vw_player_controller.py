import pygame
from pygame import event
from pygame.locals import *

import pymasep as pm

from src.game import VWGame
from src.interface import VWPlayerController


def test_select_action():
    env = pm.common.Environment()
    controller = VWPlayerController(env)
    evnt = event.Event(KEYDOWN, {"key": pygame.K_DOWN})

    event_processed = controller.select_action(evnt)

    assert event_processed
    assert controller.action.type == VWGame.ACTION_SOUTH
