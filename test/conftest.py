# content of conftest.py
import pytest

from src.game import VWGame

pytest_plugins = ["pymasep.tests.fixtures"]

@pytest.fixture(scope="function")
def game_class():
    """
    Create a game according to the current project
    """
    yield VWGame


